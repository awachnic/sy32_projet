import os
import pandas as pd
from shapely.geometry import Polygon


def calculate_iou(box1, box2):
    # box1 et box2 sont des listes ou des tuples sous la forme [x_min, y_min, x_max, y_max]
    poly1 = Polygon([(box1[0], box1[1]), (box1[2], box1[1]), (box1[2], box1[3]), (box1[0], box1[3])])
    poly2 = Polygon([(box2[0], box2[1]), (box2[2], box2[1]), (box2[2], box2[3]), (box2[0], box2[3])])
    intersection = poly1.intersection(poly2).area
    union = poly1.union(poly2).area
    return intersection / union

def calculate_metrics_one_image(detections, ground_truths, iou_threshold=0.5):
    tp, fp, fn = 0, 0, 0
    detected_gt = set()
    
    for det in detections:
        box_det = [int(det[0]), int(det[1]), int(det[0]) + int(det[2]), int(det[1]) + int(det[3])]
        class_det = det[4]
        
        max_iou = 0
        matched_gt = None
        
        for gt in ground_truths:
            box_gt = [int(gt[0]), int(gt[1]), int(gt[0]) + int(gt[2]), int(gt[1]) + int(gt[3])]
            class_gt = gt[4]
            
            iou = calculate_iou(box_det, box_gt)
            
            if iou > max_iou:
                max_iou = iou
                matched_gt = gt
        
        if max_iou >= iou_threshold and class_det == matched_gt[4]:
            tp += 1
            detected_gt.add(tuple(matched_gt))
        else:
            fp += 1

    for gt in ground_truths:
        if tuple(gt) not in detected_gt:
            fn += 1
            
    return tp, fp, fn


def calculate_metrics(true_label_folder, label_detection_folder, iou_threshold=0.5):

    tp, fp, fn = 0, 0, 0

    for i, filename in enumerate(os.listdir(label_detection_folder)):

        #print('i = ', filename)
        true_label_file = os.path.join(true_label_folder, filename)
        label_detection_file = os.path.join(label_detection_folder, filename)

        try:
            true_label_data = pd.read_csv(true_label_file, header=None).values.tolist()
        except:
            true_label_data = []

        try:
            label_detection_data = pd.read_csv(label_detection_file, header=None).values.tolist()
        except:
            label_detection_data = []

        #print(true_label_data)
        #print(label_detection_data)

        tp_img, fp_img, fn_img = calculate_metrics_one_image(label_detection_data, true_label_data)

        tp += tp_img
        fp += fp_img
        fn += fn_img

    return tp, fp, fn

def calculate_precision_recall(tp, fp, fn):
    precision = tp / (tp + fp) if (tp + fp) > 0 else 0
    recall = tp / (tp + fn) if (tp + fn) > 0 else 0
    return precision, recall
            

label_detection_folder = 'detection_label'
true_label_folder = '../dataset/val/labels'

calculate_metrics(true_label_folder, label_detection_folder)
tp, fp, fn = calculate_metrics(true_label_folder, label_detection_folder)

print(f"True Positives (TP): {tp}")
print(f"False Positives (FP): {fp}")
print(f"False Negatives (FN): {fn}")

precision, recall = calculate_precision_recall(tp, fp, fn)
print(f"Précision: {precision:.2f}")
print(f"Rappel: {recall:.2f}")