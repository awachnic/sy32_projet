#%%
import matplotlib.pyplot as plt
import os
from PIL import Image, ImageDraw, ImageFont, ImageColor
import pandas as pd
import csv
from skimage import io
import random
import numpy as np
from skimage import transform
from skimage import img_as_ubyte
import joblib
from non_maxima import delete_non_maxima_image
import time

def detection_entire_image(img_resized, label, l, display_fig = False):

    # 1ere etape : tester directement le clf (si le panneau fait la taille de l'image)
    img_reshape = transform.resize(
                img_resized, (48, 48),
                anti_aliasing=True, mode='reflect')
    
    # Convertir les valeurs des pixels à l'échelle [0, 255]
    img_reshape = img_as_ubyte(img_reshape)
    img_reshape = np.clip(img_reshape, 0, 255)

    img_flatten = img_reshape.flatten().reshape(1, -1)

    # test du clf
    result_clf = clf.predict_proba(img_flatten)[0][l]

    # si resultat superieur à un seul
    if result_clf > 0.7:

        # affiche de la fenetre
        if display_fig:
            fig, ax = plt.subplots()
            ax.imshow(img_resized)
            ax.axis('off')
            rect = plt.Rectangle((0, 0), img_resized.shape[0], img_resized.shape[1], edgecolor='red', facecolor='none')
            ax.add_patch(rect)
            plt.show()

            print(f'{label}, {result_clf:.4f}')

        # renvoie le label, la propa de prediction et les coordonnées
        return [0, 0, img_resized.shape[0], img_resized.shape[1], label, result_clf]
    
    else:
        return None
    

def recursive_sliding_windows(img_resized, labels_index, labels_name, window_size, step_size, display_fig=False):

    labels_list_detection_sliding_windows = []

    for y in range(0, img_resized.shape[0]-window_size+1, step_size):
        for i, x in enumerate(range(0, img_resized.shape[1]-window_size+1, step_size)):

            window = img_resized[y:y + window_size, x:x + window_size]
            # reshape de l'image
            window_reshape = transform.resize(
                window, (48, 48),
                anti_aliasing=True, mode='reflect')
            
            # Convertir les valeurs des pixels à l'échelle [0, 255]
            window = img_as_ubyte(window_reshape)
            window = np.clip(window, 0, 255)

            # ajout du panneau en flatten et du label
            window_flatten = window.flatten().reshape(1, -1)
            
             # test du clf
            result_clf = clf.predict_proba(window_flatten)[0]

            for l in labels_index:
                result_clf_label = result_clf[l]

                if result_clf_label > 0.9:
                    labels_list_detection_sliding_windows.append([x, y, window_size, window_size, labels_name[l], result_clf_label])

                    if display_fig:
                        # create a figure to display the image with the current window highlighted
                        fig, ax = plt.subplots()
                        ax.imshow(img_resized)
                        ax.axis('off')

                        # draw rectangle around the current window on the image
                        rect = plt.Rectangle((x, y), window_size, window_size, edgecolor='red', facecolor='none')
                        ax.add_patch(rect)
                        
                        # display the figure with the rectangle
                        plt.show()

                        print(labels_name[l], result_clf_label)


    return labels_list_detection_sliding_windows




def save_list_csv(list, filename):
    # Ouverture du fichier en mode écriture
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        
        # Écriture des données dans le fichier CSV
        for row in list:
            writer.writerow(row)

def detection_image(img_filename, label_filename, img_to_save_filename, clf, display_fig = False):
    '''detection des panneaux pour 1 image'''

    img = io.imread(img_filename)
    img_number = img_filename[-8:-4]
    print(img_number)

    # resize the image
    img_resized = transform.resize(img, (1000, 1000), anti_aliasing=True, mode='reflect')
    # create a figure to display the sliding windows
    if display_fig:
        fig, ax = plt.subplots()
        ax.imshow(img_resized)
        ax.axis('off')
        plt.show()

    # liste des noms des labels
    labels_list = clf.classes_

    labels_list_detection = []

    # recouvrement variable
    list_window_size_step = [[1, 1],
                            [0.8, 100],
                            [0.7, 100],
                            [0.6, 100],
                            [0.5, 100],
                            [0.4, 100],
                            [0.3, 100],
                            [0.2, 100],
                            [0.1, 50]]

    labels_square = ['ceder', 'danger', 'interdiction', 'obligation', 'stop']
    indices_labels_square = [i for i, label in enumerate(labels_list) if label in labels_square]

    for window_size_step in list_window_size_step:
            
            labels_list_detection_sliding_windows = recursive_sliding_windows(img_resized, indices_labels_square, labels_list, 
                                                                                 int(window_size_step[0]*img_resized.shape[0]), 
                                                                                 window_size_step[1], display_fig)
            
            labels_list_detection += labels_list_detection_sliding_windows

    labels_list_detection_non_maxima = delete_non_maxima_image(labels_list_detection, img_filename, img_to_save_filename)
    save_list_csv(labels_list_detection_non_maxima, label_filename)

    return labels_list_detection_non_maxima





def detect_all_img(val_img_folder):

    # Parcourir tous les fichiers du dossier
    for filename in os.listdir(val_img_folder):
        # Construire le chemin complet du fichier
        filepath = os.path.join(val_img_folder, filename)

        # Vérifier si c'est un fichier (et non un dossier)
        if os.path.isfile(filepath):
            
            filename_img = os.path.join(val_img_folder, filename)

            img_number = filename.split('/')[-1].split('.')[0]  # Extract image number from filename

            file_label_to_save = 'detection_label/'+ img_number + '.csv'
            file_img_to_save = 'detection_img/' + img_number + '.jpg'
            print(f'Processing file: {filename_img}', file_label_to_save, file_img_to_save)
            detection_image(filename_img, file_label_to_save, file_img_to_save, clf, display_fig = False)


 

# image test
img_test_number = '0004'
img_test_filename = '../dataset/val/images/' + img_test_number + '.jpg'
filename_label_to_save = 'detection_label/'+ img_test_number + '.csv'
filename_img_to_save = 'detection_img/'+ img_test_number + '.jpg'

# chargement du clf
clf_name = f'classifiers/svm_clf.pkl'
clf = joblib.load(clf_name)

start = time.time()
detection_image(img_test_filename, filename_label_to_save, filename_img_to_save, clf, display_fig = False)
end = time.time()
print('time : ', end-start)


'''val_img_folder = '../dataset/val/images/'
detect_all_img(val_img_folder)
'''

# %%
