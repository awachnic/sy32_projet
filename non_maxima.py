#%%
import matplotlib.pyplot as plt
import os
from PIL import Image, ImageDraw, ImageFont, ImageColor
from shapely.geometry import Polygon
import pandas as pd
import csv
from skimage import io
import random
import numpy as np
from skimage import transform
from skimage import img_as_ubyte
import joblib 
from image_visualisation import visualize_image

def recursive_delete_non_maxima_image(list_detection_img, i, debug=False):

    if debug:
        print()
        print(i)
    for row in list_detection_img:
        print(row)

    if i == len(list_detection_img):
        return list_detection_img

    # extraction de l'image de reference correspond à i
    img_ref = list_detection_img[i]
    label_ref = img_ref[4]
    x_ref, y_ref =  int(img_ref[0]), int(img_ref[1])
    window_size_x_ref, window_size_y_ref = int(img_ref[2]), int(img_ref[3])

    # initialisation de la liste qui va etre renvoyé au prochain appel
    new_list_detection_img = []
    for j in range(i+1):
        new_list_detection_img.append(list_detection_img[j])

    polygon_ref = Polygon([(x_ref, y_ref), (x_ref+window_size_x_ref, y_ref), (x_ref+window_size_x_ref, y_ref+window_size_y_ref), (x_ref, y_ref+window_size_y_ref)])

    # parcours de toutes images à partir de l'image de ref
    for j in range(i+1, len(list_detection_img)):

        x_j, y_j = int(list_detection_img[j][0]), int(list_detection_img[j][1])    
        window_size_x_j, window_size_y_j = int(list_detection_img[j][2]), int(list_detection_img[j][3])

        polygon_j = Polygon([(x_j, y_j), (x_j+window_size_x_j, y_j), (x_j+window_size_x_j, y_j+window_size_y_j), (x_j, y_j+window_size_y_j)])
        
        intersection = polygon_ref.intersection(polygon_j)
        union = polygon_ref.union(polygon_j)
        recouvrement = intersection.area / union.area

        intersection2 = polygon_j.intersection(polygon_ref)
        union2 = polygon_j.union(polygon_ref)
        recouvrement2 = intersection2.area / union2.area

        if debug:
            print(recouvrement, intersection.area, polygon_j.area, label_ref, list_detection_img[j][4])

        #print(recouvrement, label_ref, list_detection_img[j][4])

        if label_ref == list_detection_img[j][4]:
            if recouvrement <= 0:
                new_list_detection_img.append(list_detection_img[j])
        else:  
            #if recouvrement < 0.5 and intersection.area != polygon_j.area:
            if recouvrement < 0.5 and intersection.area <= 0.5*polygon_j.area:
                new_list_detection_img.append(list_detection_img[j])

    if debug:
        for row in new_list_detection_img:
            print(row)
    # condition d'arret
    return recursive_delete_non_maxima_image(new_list_detection_img, i+1)
    

def correct_coordinate(img_detection_non_maxima, filename_img, img_to_save_filename):

    img = io.imread(filename_img)
    original_shape = img.shape[:2]  # (height, width)

    #img_resized = transform.resize(img, (1000, 1000), anti_aliasing=True, mode='reflect')

    fig, ax = plt.subplots()
    ax.imshow(img)
    ax.axis('off')

    # Facteurs de redimensionnement
    resize_factor_y = original_shape[0] / 1000
    resize_factor_x = original_shape[1] / 1000

    for row in img_detection_non_maxima:
        # conversion des coordonnées en x_min, y_min, x_max, y_max
        x_resized, y_resized, length_resized, height_resized = map(int, row[0:4])
        xmax_resized, ymax_resized = x_resized + length_resized, y_resized + height_resized
        
        # Reconversion aux coordonnées d'origine
        x_original = int(x_resized * resize_factor_x)
        y_original = int(y_resized * resize_factor_y)
        xmax_original = int(xmax_resized * resize_factor_x)
        ymax_original = int(ymax_resized * resize_factor_y)
        length_original = xmax_original - x_original
        height_original = ymax_original - y_original

        # Mise à jour des coordonnées dans row
        row[0] = x_original
        row[1] = y_original
        row[2] = x_original + length_original
        row[3] = y_original + height_original

        # Affichage des rectangles et labels sur l'image redimensionnée
        #rect = plt.Rectangle((x_original, y_original), length_original, height_original, edgecolor='red', facecolor='none')
        rect = plt.Rectangle((x_original, y_original), length_original, height_original, edgecolor='red', facecolor='none')
        ax.add_patch(rect)

        ax.text(x_original, y_original, row[4], fontsize=12, color='black')

    #img_number = filename_img.split('/')[-1].split('.')[0]  # Extract image number from filename
    plt.savefig(img_to_save_filename, dpi=400) 
    plt.show()

    return img_detection_non_maxima

def delete_non_maxima_image(list_label_detection, filename_img, img_to_save_filename):

    # ouverture du fichier 
    # Liste pour stocker les données

    # trie des detections par score decroissant
    list_label_detection.sort(key=lambda x: float(x[5]), reverse=True)
    
    img_detection_non_maxima = recursive_delete_non_maxima_image(list_label_detection, 0)
    img_detection_non_maxima_corrected = correct_coordinate(img_detection_non_maxima, filename_img, img_to_save_filename)

    return img_detection_non_maxima_corrected


'''list_label_teeest = [[200, 100, 700, 700, 'stop', 0.9974830641041149],
                    [300, 200, 500, 500, 'stop', 0.9900314246141778],
                    [200, 200, 600, 600, 'stop', 0.9862852609792225],
                    [200, 100, 600, 600, 'stop', 0.9649855441114199],
                    [400, 200, 400, 400, 'interdiction', 0.9507023520989435],
                    [200, 0, 800, 800, 'stop', 0.9464082656722499],
                    [300, 100, 700, 700, 'stop', 0.9459781387469592],
                    [550, 450, 100, 100, 'ceder', 0.9373372359823867],
                    [400, 400, 300, 300, 'interdiction', 0.9363888266681144],
                    [100, 100, 800, 800, 'stop', 0.9346748199486705],
                    [300, 100, 600, 600, 'stop', 0.9178643454448299],
                    [400, 0, 400, 400, 'interdiction', 0.8805757246813993],
                    [300, 200, 600, 600, 'stop', 0.8763000217323508],
                    [200, 100, 800, 800, 'stop', 0.8620540274577939],
                    [500, 400, 200, 200, 'interdiction', 0.8372469759236482]]

img_nb = '0088'''

list_label_teeest = [[450, 300, 100, 100, 'obligation', 0.9999906621901167],
                    [550, 400, 100, 100, 'obligation', 0.999963517685262],
                    [550, 350, 100, 100, 'obligation', 0.9973218735020954],
                    [300, 100, 500, 500, 'interdiction', 0.9839252118310612],
                    [400, 400, 100, 100, 'obligation', 0.9806817352493494],
                    [300, 400, 300, 300, 'interdiction', 0.9385380001631232],
                    [300, 400, 500, 500, 'interdiction', 0.9349085228649822],
                    [500, 400, 200, 200, 'ceder', 0.9061843763039832],
                    [550, 500, 100, 100, 'interdiction', 0.9030371809030765],
                    [300, 200, 400, 400, 'interdiction', 0.8812241588958521],
                    [500, 300, 300, 300, 'interdiction', 0.8762863798122683],
                    [200, 100, 600, 600, 'interdiction', 0.8405004195360581],
                    [200, 200, 500, 500, 'interdiction', 0.8358638267772621],
                    [400, 100, 400, 400, 'interdiction', 0.8335847711656842],
                    [300, 100, 600, 600, 'interdiction', 0.8121733725724792],
                    [400, 100, 600, 600, 'interdiction', 0.8017111459056181]]

'''img_nb = '0101'

filename_label_test = 'detection/' + img_nb + '.csv'
filename_img_test = '../dataset/val/images/'+ img_nb + '.jpg'

delete_non_maxima_image(list_label_teeest, filename_img_test)'''

