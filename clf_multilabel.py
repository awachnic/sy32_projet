#%%
import os
import pandas as pd
from generation_multilabel_dataset import multilabel_dataset
from sklearn.svm import SVC
import joblib

def create_classifier(X_train, y_train, clf_name):
    '''
    Creer un classifieur pour un type de panneaux
    '''

    svm_clf = SVC(kernel='linear', probability=True)
    svm_clf.fit(X_train, y_train)
    joblib.dump(svm_clf, clf_name)

def test_classifieur(X_val, y_val, clf_name):
    '''
    Test le classfieur sur les données val
    '''

    svm_clf = joblib.load(clf_name)
    score = svm_clf.score(X_val, y_val)
    print('Score : ', score)


# nom des dossiers pour train
img_train = '../dataset/train/images/'
label_train = '../dataset/train/labels/'

# nom des dossiers pour val
img_val = '../dataset/val/images/'
label_val = '../dataset/val/labels/'

# train
X_train, y_train = multilabel_dataset(img_train, label_train)

clf_name = f'classifiers/svm_clf.pkl'

create_classifier(X_train, y_train, clf_name)

# Val

X_val, y_val = multilabel_dataset(img_val, label_val)


test_classifieur(X_val, y_val, clf_name)
