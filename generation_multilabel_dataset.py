#%%

import matplotlib.pyplot as plt
import os
from PIL import Image, ImageDraw, ImageFont, ImageColor
import pandas as pd
import csv
from skimage import io
import random
import numpy as np
from skimage import transform
from skimage import img_as_ubyte

# Fonction pour vérifier si la première ligne d'un fichier CSV est vide
def is_csv_first_line_empty(file_path):
    '''
    Verifie si un fichier .csv de label est vide 
    Renvoie vrai si le fichier est vide
    '''

    with open(file_path, 'r') as file:
        reader = csv.reader(file)
        first_line = next(reader, None)  # Lire la première ligne
        if first_line is None:  # Vérifier si le fichier est complètement vide
            return True
        # Vérifier si tous les champs de la première ligne sont vides
        return all(field.strip() == '' for field in first_line)

# fonction qui affiche 
def plot_traffic_sign_img(img, name):
    fig, ax = plt.subplots()
    ax.imshow(img)
    ax.set_title(name)
    ax.axis('off')
    plt.show()

# fonction qui génère une 
def generate_random_image(img, img_traffic_sign):
    '''
    genere une image aléatoire dans img de la même taille que img_traffic_sign
    '''

    # Génération d'une partie aléatoire de l'image de même taille
    height, width, _ = img_traffic_sign.shape
    img_height, img_width, _ = img.shape

    # Assurez-vous que la partie aléatoire se trouve dans les limites de l'image
    max_x = img_width - width
    max_y = img_height - height

    if max_x > 0 and max_y > 0:
        random_x = random.randint(0, max_x)
        random_y = random.randint(0, max_y)

        img_random = img[random_y:random_y + height, random_x:random_x + width]

    return img_random

def multilabel_dataset(img_folder, label_folder, nb_images=None,
                   plot_traffic_sign=False):
    '''Renvoie les images et labels d'un jeu de données
    Genère aléatoirement '''

    labels_list = ['empty',
                   'danger',
                    'interdiction',
                    'obligation',
                    'stop',
                    'ceder',
                    'frouge',
                    'forange',
                    'fvert',]

    if not nb_images:
        # recuperation des fichiers des images
        images_file = [os.path.join(img_folder, l)
                       for l in os.listdir(img_folder)]

        # recuperation des fichiers des labels
        labels_file = [os.path.join(label_folder, l)
                       for l in os.listdir(label_folder)]
    else:
        # recuperation des fichiers des images
        images_file = [os.path.join(img_folder, l)
                       for l in os.listdir(img_folder)[:nb_images]]

        # recuperation des fichiers des labels
        labels_file = [os.path.join(label_folder, l)
                       for l in os.listdir(label_folder)[:nb_images]]
        
    # initialisation des listes qui vont contenir les images
    images_traffic_sign = []
    labels_traffic_sign = []

    for i, label_file in enumerate(labels_file):

        try:
            # Vérifier que le fichier de labels n'est pas vide
            if not is_csv_first_line_empty(label_file):

                # lecture des labels
                labels = pd.read_csv(label_file, header=None)

                # lecture de l'image
                # img = Image.open(images_file[i])
                img = io.imread(images_file[i])

                # parcours de chaque label
                for index, row in labels.iterrows():

                    # lecture du label du panneau
                    traffic_sign_name = row[4]

                    img_traffic_sign = img[row[1]:row[3], row[0]:row[2]]
                    # reshape de l'image
                    img_traffic_sign = transform.resize(
                        img_traffic_sign, (48, 48),
                        anti_aliasing=True, mode='reflect')
                    
                    # Convertir les valeurs des pixels à l'échelle [0, 255]
                    img_traffic_sign = img_as_ubyte(img_traffic_sign)
                    img_traffic_sign = np.clip(img_traffic_sign, 0, 255)

                    # ajout du panneau en flatten et du label
                    images_traffic_sign.append(img_traffic_sign.flatten())
                    labels_traffic_sign.append(traffic_sign_name)

                    # pour chaque image avec un panneau, on rajoute 2 images
                    # sans panneau
                    for _ in range(3):
                        try:
                            img_random = generate_random_image(
                                img, img_traffic_sign)
                            images_traffic_sign.append(
                                img_random.flatten())
                            labels_traffic_sign.append('ff')

                            if plot_traffic_sign:
                                plot_traffic_sign_img(img_random, 'Random')

                        except:
                                    continue
                    # affichage du panneau et du nom
                    if plot_traffic_sign:
                        plot_traffic_sign_img(
                            img_traffic_sign, traffic_sign_name)
              
        except:
            print('Erreur sur lecture du fichier label.csv ; image suivant')

    return images_traffic_sign, labels_traffic_sign

